<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    //

    public function doctor(){
        return $this->belongsTo(Doctors::class);
    }

    public function hospital()
    {
        return $this->belongsTo(Hospitals::class);
    }
    protected $fillable=['doctor_id','hospital_id','day_id','interval','start','end'];

//    public function da
}

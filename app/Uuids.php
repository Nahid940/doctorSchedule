<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 12/9/2017
 * Time: 11:49 PM
 */

namespace App;
use Webpatser\Uuid\Uuid;


trait Uuids
{

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Uuid::generate()->string;
        });
    }

}
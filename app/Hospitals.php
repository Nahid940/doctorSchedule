<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hospitals extends Model
{
    //
    public  function doctors(){
        return $this->belongsToMany(Doctors::class,'doctors_hospitals');
    }
}

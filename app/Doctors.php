<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Doctors extends Authenticatable
{
    //
    use Uuids;
    public $incrementing =false;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hospitals()
    {
        return $this->belongsToMany(Hospitals::class,'doctors_hospitals');
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }


}

<?php

namespace App\Http\Controllers;

use App\Doctors;
use App\Schedule;
use Illuminate\Http\Request;

class GetDoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data=Doctors::select('id','name')->get();
        return view('alldoctors',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

//    public function getParticularDoctor($id,$name)
//    {
//        $data=Schedule::select('hospital_name','name','hospital_id','doctor_id','start','end','interval','day_name')
//            ->leftJoin('hospitals','schedules.hospital_id','=','hospitals.id')
//            ->leftJoin('doctors','schedules.doctor_id','=','doctors.id')
//            ->leftJoin('days','schedules.day_id','=','days.id')
//            ->where('schedules.doctor_id','=',$id)
//            ->distinct()
//            ->get();
//            return view('getDoctornInfor',compact('data','name'));
//    }
    public function getParticularDoctor($id)
    {
        $data=Schedule::select('hospital_name','name','hospital_id','doctor_id')
            ->leftJoin('hospitals','schedules.hospital_id','=','hospitals.id')
            ->leftJoin('doctors','schedules.doctor_id','=','doctors.id')
            ->where('schedules.doctor_id','=',$id)
            ->distinct()
            ->get();
            return view('getDoctornInfor',compact('data'));
    }

    public function DoctorOfSpecificHOpistal($doctorid,$id)
    {
        $data=Schedule::select('hospital_name','name','hospital_id','doctor_id','start','end','day_id')
            ->leftJoin('hospitals','schedules.hospital_id','=','hospitals.id')
            ->leftJoin('doctors','schedules.doctor_id','=','doctors.id')
            ->leftJoin('days','schedules.day_id','=','days.id')
            ->where('schedules.doctor_id','=',base64_decode($doctorid))
            ->where('schedules.hospital_id','=',$id)
            ->get();
        //return $data;
        //return view('/DoctorOfSpecificHOpistal/'.$doctorid.'/'.$id);
    }
}

<?php

namespace App\Http\Controllers\Appointment;

use App\Schedule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function MongoDB\BSON\toJSON;
use Psy\Util\Json;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       return $request;


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getScheduleData(Request $request)
    {
        $option='';
        $data=Schedule::select('hospital_id','doctor_id','days.id as dayId','value','day_name','start','end','interval')
            ->leftJoin('hospitals','schedules.hospital_id','=','hospitals.id')
            ->leftJoin('doctors','schedules.doctor_id','=','doctors.id')
            ->leftJoin('days','schedules.day_id','=','days.id')
            ->where('schedules.doctor_id','=',$request->doctor_id)
            ->where('schedules.hospital_id','=',$request->hospital_id)
            ->get();

        if(sizeof($data)!=0){
            foreach ($data as $d){
                $interval = date_interval_create_from_date_string($d->interval . ' minutes');
                $begin = date_create($d->start);
                $end = date_create($d->end)->add($interval);

                $option .="<div class='row' ><div class='col-md-12 schedule' name='sadas'  id='$d->day_name'><h3>Select time for ".$d->day_name."</h3>";
                foreach (new \DatePeriod($begin, $interval, $end) as $dt) {
                      $option .= "<li style='list-style: none; margin-top:5px '><button class='btn btn-info' id='cnfrm' data-time=".$dt->format('h:i:a').">".$dt->format('h:i:a')."</button></li>";
                };
                $option .="</div></div>";
            }
        }

        return array($data,$option);
    }

//    public function dataSchedule(Request $request){
//        $option='';
//        $data=Schedule::select('day_name','start','end')
//            ->leftJoin('hospitals','schedules.hospital_id','=','hospitals.id')
//            ->leftJoin('doctors','schedules.doctor_id','=','doctors.id')
//            ->leftJoin('days','schedules.day_id','=','days.id')
//            ->where('schedules.doctor_id','=',$request->doctor_id)
//            ->where('schedules.hospital_id','=',$request->hospital_id)
//            ->where('schedules.day_id','=',$request->day_id)
//            ->get();
//        $datas=array();
//
//        foreach($data as $d) {
//            $interval = date_interval_create_from_date_string($d->interval . ' minutes');
//            $begin = date_create($d->start);
//            $end = date_create($d->end)->add($interval);
//            foreach (new \DatePeriod($begin, $interval, $end) as $dt) {
//                 //$option .= "<tr><td><button id='timeSlot' data-slot_id=".$dt->format('h:i:a')." data-day_id=".$d->value." class='btn btn-info'>".$dt->format('h:i:a')."</button></td></tr>";
//                  //  $option .= "<button class='btn btn-info'>".$dt->format('h:i:a')."</button>";
//               //     $option.="<ul style='float:left;list-style: none;'><li><button  class='btn btn-info' id='timeSlot' data-slot_id=".$dt->format('h:i:a')." data-day_id=".$d->value."  data-day_name=".$d->day_name." data-toggle='modal' data-target='#myModal'>".$dt->format('h:i:a')."</button></li></ul>";
//                $datas=$dt->format('h:i:a');
//               };
//
//            //$option.="<thead><tr><th>".$d->day_name."</th><tr></thead>";
////            $option.="<div class='col-md-12'><div class='row'><h3>$d->day_name</h3>";
//
////                foreach (new \DatePeriod($begin, $interval, $end) as $dt) {
////                 //$option .= "<tr><td><button id='timeSlot' data-slot_id=".$dt->format('h:i:a')." data-day_id=".$d->value." class='btn btn-info'>".$dt->format('h:i:a')."</button></td></tr>";
////                  //  $option .= "<button class='btn btn-info'>".$dt->format('h:i:a')."</button>";
////                    $option.="<ul style='float:left;list-style: none;'><li><button  class='btn btn-info' id='timeSlot' data-slot_id=".$dt->format('h:i:a')." data-day_id=".$d->value."  data-day_name=".$d->day_name." data-toggle='modal' data-target='#myModal'>".$dt->format('h:i:a')."</button></li></ul>";
////                };
////                $option.="</div></div>";
////               "</tbody>";
////               "</table>";
//            };
//
//        //return $option;
//
//        return $datas;
//    }
}

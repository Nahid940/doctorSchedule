<?php

namespace App\Http\Controllers;

use App\Doctors;
use App\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Integer;

class DoctorSchedule extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $day=$request->input('day');
        $hospital_id=$request->input('hospital_id');

        $start=$request->input('start');
        $end=$request->input('end');
        $intr=$request->input('duration');
        for($i=0;$i<sizeof($day);$i++) {
            $data = count(Schedule::select('doctor_id', 'hospital_id', 'day_id')
                ->where('doctor_id', '=', $request->id)
                ->where('hospital_id', '=', $hospital_id)
                ->where('day_id', '=', $day[$i])
                ->where('day_id', '=', $day[$i])
                ->where('start', '=', $start[$day[$i] - 1])
                ->where('end', '=', $end[$day[$i] - 1])
                ->get());

            if ($data != 0) {
                return redirect('/doctor/insert')->with('dataexist', 'Data already exist');
            } else {
                Schedule::create(['doctor_id' => $request->id, 'hospital_id' => $hospital_id, 'day_id' => $day[$i], 'interval' => $intr[$day[$i] - 1], 'start' => $start[$day[$i] - 1], 'end' => $end[$day[$i] - 1]]);
                return redirect('/doctor/insert');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

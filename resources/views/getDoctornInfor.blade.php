<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>

<style>

    .schedule{
        display: none;
    }

</style>


<body>

<div class="container-fluid">

    <div class="row">
        <div class="col-md-6">
            <label for="">
                @php
                    echo $data[0]->name."<br/>";
                @endphp
                <br/>
            </label>


            <div id="chamberlist">
                <table class="table">
                    @foreach($data as $d)
                        <tr>
                            <td><a class="btn btn-primary" data-id={{$d->doctor_id}} data-id2={{$d->hospital_id}} id='viewschedule'>Chamber : {{$d->hospital_name}}</a></td>
                        </tr>
                    @endforeach
                </table>
            </div>

        </div>
    </div>


        <div class="row">
            <div class="col-lg-8" id="datetime">

                {{--<form action="" method="post">--}}
                    <div class="col-md-4">

                        <div class="datetime">

                        </div>
                    </div>
                    <div class="col-md-4">

                        <div  id="doctorSchedule">

                        </div>
                    </div>
                {{--</form>--}}
            </div>

            <div class="col-lg-8" id="appointment">


            </div>

        </div>

</div>

<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
    var dayNameId=[];
    var dct;
    var dayId;
    var doctorID;
    var hospitalID;
    var dayOfWeek;
    var date;

    var weekday=new Array();
    weekday['Mon']="Monday";
    weekday['Tue']="Tuesday";
    weekday['Wed']="Wednesday";
    weekday['Thu']="Thursday";
    weekday['Fri']="Friday";
    weekday['Sat']="Saturday";
    weekday['Sun']="Sunday";

     var token='{{\Illuminate\Support\Facades\Session::token()}}';

     $(document).ready(function () {
         // dayNameId=[];
     })

    $(document).on('click','#viewschedule',function () {
        $('#doctorSchedule').empty();
        dayNameId=[];
        var doctor_id=$(this).data('id');
        var hospital_id=$(this).data('id2');
        dct=doctor_id;
        $.ajax({
            method:'post',
            url:'{{route('myappointment')}}',
            data:{
                _token:token,
                doctor_id:doctor_id,
                hospital_id:hospital_id
            },
            success:function (data) {
                for (i = 0; i < data[0].length; i++) {
                    dayNameId[i] = data[0][i]['value'];
                }
                $('.datetime').html('<label>Select date : </label><input type="text" id="txtDate" name="apptDate" class="form-control">');
                $('#doctorSchedule').append(data[1]);

                $(function () {
                    $('#txtDate').datepicker({
                        beforeShowDay:
                            function (dt) {
                                dmy = dt.getDay();
                                if ($.inArray(dmy, dayNameId) == -1) {
                                    return [false, "Unavailable"];
                                } else {
                                    return [true, "", "Available"];
                                }
                            },
                        minDate: 0,
                        maxDate: '+20d',
                        showAnim: 'clip',
                        dateFormat: 'yy-mm-dd',
                        onSelect: function (inst) {

                            var seldate = $(this).datepicker('getDate');
                            seldate = seldate.toDateString();
                            seldate = seldate.split(' ');
                            dayOfWeek = weekday[seldate[0]];
                            date=$("#txtDate[name=apptDate]").val();

                           $('#Monday').addClass('schedule');
                           $('#Wednesday').addClass('schedule');
                           $('#Sunday').addClass('schedule');
                           $('#Saturday').addClass('schedule');
                           $('#Tuesday').addClass('schedule');
                           $('#Thursday').addClass('schedule');
                           $('#Friday').addClass('schedule');
                           $('#'+dayOfWeek).removeClass('schedule');
                        }
                    });
                });
            }
        })
    });
     
     function formValidate() {
         // var name=document.forms["appointmentForm"]["name"].value;
         if(document.forms["appointmentForm"]["name"].value==''){
             alert('Insert Name');
             return false;
         }else if(document.forms["appointmentForm"]["phn"].value=='' || isNaN(document.forms["appointmentForm"]["phn"].value)){
             alert('Insert phone');
             return false;
         }
         return true;
     }

     $(document).on('click','#cnfrm',function () {

         // $('#appointment').empty();
         $('#datetime').hide();
         $('#chamberlist').hide();
         $('#appointment').html("<div class='col-md-6'>" +
             "<form method='post' action='{{route('appnt')}}' name='appointmentForm' onsubmit='return formValidate()'>" + '{{csrf_field()}}'+
             "<h3>Appointment on "+dayOfWeek+" "+date+ " at "+$(this).data('time')+"</h3>"+
             "<div class='form-group'>"+
             "<label for='Name'>Your name:</label>"+
             "<input type='text' class='form-control' id='name' name='name'>"+
             "</div>"+

             "<div class='form-group'>"+
             "<label for='email'>Email address:</label>"+
             "<input type='email' class='form-control' name='email' id='email'>"+
             "</div>"+

             "<div class='form-group'>"+
             "<label for='phn'>Your Phone No.:</label>"+
             "<input type='text' class='form-control' name='phn' id='phn'>"+
             "</div>"+

             "<div class='form-group'>"+
             "<input type='submit' class='btn btn-success pull-right' value='Book appointment' id='submit'>"+

             "</div>"+
             "<div class='form-group'>"+
             "<input type='button' class='btn btn-info pull-right' value='Go back' id='goback'>"+
             "</div>"+

             "</form>" +
             "</div>");
     });

     $(document).on('click','#goback',function () {
         $('#appointment').empty();
         $('#datetime').show();
         $('#chamberlist').show();
     })

</script>
</body>
</html>
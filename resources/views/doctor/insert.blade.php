@extends('doctor.layout.master')
@section('content')


    <div class="row">

        <div class="col-lg-12">
            <div class="col-lg-6 col-lg-offset-3">

                @php
                   $hospitals= \App\Http\Controllers\HospitalController::getAllHospitals();
                    $days=\App\Http\Controllers\Days::AllDays();
                @endphp
                @if(session('failed'))
                    {{session('failed')}}
                @endif

                @if(session('dataexist'))
                    <div class="alert alert-danger">{{session('dataexist')}}</div>
                @endif

                <form action="{{route('storeSchedule')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="sel1">Select hospital:</label>
                        <select class="form-control" id="hospital" name="hospital_id">
                            @foreach($hospitals as $h)
                                <option value="{{$h->id}}">{{$h->hospital_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="sel1">Select Days:</label>
                        @foreach($days as $d)
                            <div class="checkbox">
                                <label><input type="checkbox" name="day[]" value="{{$d->id}}">{{$d->day_name}}</label>
                            </div>
                            <div class="form-group">
                                <label for="">Time:</label>
                                    <table class="table">
                                        <tr>
                                            <td><label for="sel1">Select start time:</label>
                                                <select class="form-control" id="" name="start[]">
                                                    <option value="">Start</option>
                                                    <option value="10:00">10 am</option>
                                                    <option value="11:00">11 am</option>
                                                    <option value="12:00">12 am</option>
                                                    <option value="13:00">1 pm</option>
                                                    <option value="14:00">2 pm</option>
                                                    <option value="15:00">3 pm</option>
                                                    <option value="16:00">4 pm</option>
                                                    <option value="17:00">5 pm</option>
                                                    <option value="18:00">6 pm</option>
                                                    <option value="19:00">7 pm</option>
                                                    <option value="20:00">8 pm</option>
                                                    <option value="21:00">9 pm</option>
                                                    <option value="22:00">10 pm</option>
                                                </select>
                                            </td>
                                            <td><label for="sel1">Select end time:</label>
                                                <select class="form-control" name="end[]">
                                                    <option value="">End</option>
                                                    <option value="10:00">10 am</option>
                                                    <option value="11:00">11 am</option>
                                                    <option value="12:00">12 am</option>
                                                    <option value="13:00">1 pm</option>
                                                    <option value="14:00">2 pm</option>
                                                    <option value="15:00">3 pm</option>
                                                    <option value="16:00">4 pm</option>
                                                    <option value="17:00">5 pm</option>
                                                    <option value="18:00">6 pm</option>
                                                    <option value="19:00">7 pm</option>
                                                    <option value="20:00">8 pm</option>
                                                    <option value="21:00">9 pm</option>
                                                    <option value="22:00">10 pm</option>
                                                </select>
                                            </td>
                                            <td>
                                                <label for="sel1">Duration</label>
                                                <select class="form-control" name="duration[]">
                                                    <option value="">Duration</option>
                                                    <option value="15">15 min</option>
                                                    <option value="20">20 min</option>
                                                    <option value="30">30 min</option>
                                                </select>
                                            </td>
                                        </tr>

                                    </table>
                            </div>
                        @endforeach
                    </div>
                    <input type="hidden" value="{{\Illuminate\Support\Facades\Auth::id()}}" name="id">

                    <input type="submit" class="btn btn-info" value="Add">
                </form>

            </div>
        </div>

    </div>


@endsection
@section('script')


@endsection
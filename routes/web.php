<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('doctor/index','DoctorController@index');
Route::GET('doctor/','DoctorAuth\LoginController@showLoginForm')->name('doctor.login');
Route::POST('doctor/login', 'DoctorAuth\LoginController@login');
Route::POST('password/email','DoctorAuth\ForgotPasswordController@sendResetLinkEmail');
Route::GET('password/reset','DoctorAuth\ForgotPasswordController@showLinkRequestForm');
Route::POST('password/reset','DoctorAuth\ResetPasswordController@reset ');
Route::GET('password/reset/{token}','DoctorAuth\ResetPasswordController@showResetForm');
Route::GET('doctor/registration','DoctorAuth\RegisterController@showRegistrationForm')->name('doctor.register');
Route::post('doctor/register','DoctorAuth\RegisterController@register')->name('doctorRegistration');
Route::POST('doctor/logout','DoctorAuth\LoginController@logout')->name('Doctorlogout');

//Route::POST('register','DoctorAuth\RegisterController@register');


Route::get('doctor/insert','DoctorController@insert');
Route::post('doctor/insert/store','DoctorSchedule@store')->name('storeSchedule');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/alldoctors', 'GetDoctorController@index');
Route::get('/getDoctornInfor/{id}', 'GetDoctorController@getParticularDoctor');
Route::get('DoctorOfSpecificHOpistal/{doctorid}/{id}', 'GetDoctorController@DoctorOfSpecificHOpistal');


//Doctor appointment
Route::post('/','Appointment\AppointmentController@getScheduleData')->name('myappointment');
//Route::post('/scheduleData','Appointment\AppointmentController@dataSchedule')->name('scheduleData');
Route::post('/appnt','Appointment\AppointmentController@store')->name('appnt');


